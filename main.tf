# main.tf ######################################################################

# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
variable "hcloud_token" {}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = "${var.hcloud_token}"
}

# Create a server ######################
resource "hcloud_server" "test-hetzner" {
  name        = "test-deb-12"
  image       = "debian-12"
  server_type = "cx11"
  user_data = file("./cloud.yml")
}

# Create a volume ######################
resource "hcloud_volume" "storage" {
  name       = "test-volume"
  size       = 10	# 50
  server_id  = "${hcloud_server.test-hetzner.id}"
  automount  = true
  format     = "ext4"
}
