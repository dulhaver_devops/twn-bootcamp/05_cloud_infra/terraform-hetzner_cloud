# outputs.tf ###################################################################

output "ip_address" {
# value = digitalocean_droplet.module-5-cloud-infra.ipv4_address
  value = hcloud_server.test-hetzner.ipv4_address
  description = "The public IP address of your hetzner cloud server."
}

